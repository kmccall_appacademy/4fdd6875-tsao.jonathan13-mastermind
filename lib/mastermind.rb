class Code
  attr_reader :pegs, :PEGS

  PEGS = {
    r: "r",
    g: "g",
    b: "b",
    y: "y",
    o: "o",
    p: "p"
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random #factory method
    choices = PEGS.values
    r_keys = [rand(0..5), rand(0..5), rand(0..5), rand(0..5)]
    code_keys = [choices[r_keys[0]], choices[r_keys[1]], choices[r_keys[2]], choices[r_keys[3]]]
    Code.new(code_keys)
  end

  def self.parse(input) #factory method
    code = input.downcase.split("").map do |char|
      raise "parse method error" unless PEGS.has_key?(char.to_sym)
      PEGS[char.to_sym]
    end

    Code.new(code)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(code)
    count = 0
    @pegs.each_with_index do |peg, idx|
      count += 1 if peg == code.pegs[idx]
    end
    count
  end

  def near_matches(code)
    count = 0
    code1 = Hash.new(0)
    @pegs.each { |k| code1[k] += 1 }
    code2 = Hash.new(0)
    code.pegs.each { |k| code2[k] += 1 }

    code1.each do |k, v|
      if code2[k] != 0
        count += code2[k] if v >= code2[k]
        count += v if v < code2[k]
      end
    end

    count - exact_matches(code)
  end

  def ==(code)
    return false if code.class != Code
    return false if exact_matches(code) != 4
    true
  end

end

class Game
  attr_reader :secret_code, :guess

  def initialize(code = Code.random)
    @secret_code = code
    p "Great! Let's start!"
  end

  def get_guess
    puts "What is your guess?"
    @guess = Code.parse($stdin.gets.chomp)
  end

  def display_matches(guess)
    exact = @secret_code.exact_matches(guess)
    near = @secret_code.near_matches(guess)
    puts "You have #{exact} exact matches!"
    puts "You have #{near} near matches!"
  end

  def play
    won = false
    until won
      get_guess
      display_matches(@guess)
      won = true if @secret_code==(@guess)
    end
    puts "Congrats, you won!"
  end
end
